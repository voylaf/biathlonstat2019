ThisBuild / organization := "com.livejournal.voylaf"
ThisBuild / scalaVersion := "2.12.8"
ThisBuild / version := "0.1.0"

val pdfbox = "org.apache.pdfbox" % "pdfbox" % "2.0.14"
val postgres = "org.postgresql" % "postgresql" % "42.2.5"
val scalatest = Seq(
  "org.scalactic" %% "scalactic" % "3.0.5",
  "org.scalatest" %% "scalatest" % "3.0.5" % "test")
val scalikejdbc = Seq(
  "org.scalikejdbc" %% "scalikejdbc" % "3.3.2",
  "org.scalikejdbc" %% "scalikejdbc-test" % "3.3.2" % "test",
  "ch.qos.logback" % "logback-classic" % "1.2.3"
)
val shapeless = "com.chuusai" %% "shapeless" % "2.3.3"
val libs = scalatest ++ scalikejdbc ++ Seq(pdfbox, postgres, shapeless)

lazy val root = (project in file("."))
  .settings(
    name := "BiathlonStat2019",
    libraryDependencies ++= libs,
    scalacOptions += "-Ypartial-unification"
  )