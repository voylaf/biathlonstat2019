package voylaf.biathlonStat.biathlon

sealed trait CompetitionFormat {
  def shootings: Int

  def shootingProne: List[Int]

  def shootingStanding: List[Int]

  def lapsCount: Int
}

case object Sprint extends CompetitionFormat {
  def shootings: Int = 2

  override def shootingProne: List[Int] = List(1)

  override def shootingStanding: List[Int] = List(2)

  override def lapsCount: Int = 3
}

case object Individual extends CompetitionFormat {
  def shootings: Int = 4

  override def shootingProne: List[Int] = List(1, 3)

  override def shootingStanding: List[Int] = List(2, 4)

  override def lapsCount: Int = 5
}

case object Pursuit extends CompetitionFormat {
  def shootings: Int = 4

  override def shootingProne: List[Int] = List(1, 2)

  override def shootingStanding: List[Int] = List(3, 4)

  override def lapsCount: Int = 5
}

case object MassStart extends CompetitionFormat {
  def shootings: Int = 4

  override def shootingProne: List[Int] = List(1, 2)

  override def shootingStanding: List[Int] = List(3, 4)

  override def lapsCount: Int = 5
}

case object Relay extends CompetitionFormat {
  def shootings: Int = 8

  override def shootingProne: List[Int] = List(1, 3, 5, 7)

  override def shootingStanding: List[Int] = List(2, 4, 6, 8)

  override def lapsCount: Int = 12
}

case object MixedRelay extends CompetitionFormat {
  def shootings: Int = 8

  override def shootingProne: List[Int] = List(1, 3, 5, 7)

  override def shootingStanding: List[Int] = List(2, 4, 6, 8)

  override def lapsCount: Int = 12
}
