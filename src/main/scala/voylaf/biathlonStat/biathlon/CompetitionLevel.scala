package voylaf.biathlonStat.biathlon

trait CompetitionLevel

case object WorldCup extends CompetitionLevel

case object WorldChampionship extends CompetitionLevel

case object RussianCup extends CompetitionLevel

case object RussianChampionship extends CompetitionLevel

case object Olympics extends CompetitionLevel

case object IBUCup extends CompetitionLevel

case object EuropeanChampionship extends CompetitionLevel

