package voylaf.biathlonStat.biathlon

case class CompetitorResult(
                             surname: String,
                             name: String,
                             region: String,
                             penalty: List[Int],
                             timeTotal: DeciSecond,
                             timeRun: List[DeciSecond],
                             timeShooting: List[DeciSecond],
                             position: Int,
                             isTimeShootingValid: Boolean = false) {
  override def toString = s"""$position. $surname $name $region penalty = ${penalty.mkString("+")} timeTotal = $timeTotal ${timeRun.mkString("/")} ${timeShooting.mkString("/")}"""
}
