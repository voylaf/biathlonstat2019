package voylaf.biathlonStat.biathlon

import scala.util.{Failure, Success, Try}

case class CompetitorResultStat(
                                 competitorResult: CompetitorResult,
                                 runningPosition: Int = -1,
                                 penaltyProne: List[Int] = Nil,
                                 penaltyStandings: List[Int] = Nil,
                                 timeProne: List[DeciSecond] = Nil,
                                 timeStandings: List[DeciSecond] = Nil) {
}

case class CompetitorResultStat2(
                                  finalPositionStr: String,
                                  startPositionStr: String,
                                  surname: String,
                                  name: String,
                                  region: String,
                                  penalty: String,
                                  timeStr: String
                                ) {
  private def toInt(si: String) = Try(si.toInt) match {
    case Success(i) => i
    case Failure(exception) =>
      println(s"Ошибка приведения к целому числу для $si") // + exception.printStackTrace()
      0
  }

  val finalPosition: Int = toInt(finalPositionStr)

  val startPosition: Int = toInt(startPositionStr)

  val penaltyList: List[Int] = penalty.split(" ").toList.map(toInt)

  val time: DeciSecond = DeciSecond(timeStr)
}
