package voylaf.biathlonStat.biathlon

sealed trait Competitors {
  def isMale: Boolean

  def isAdult: Boolean
}

case object Men extends Competitors {
  val isMale = true
  val isAdult = true
}

case object Women extends Competitors {
  val isMale = false
  val isAdult = true
}

case object JuniorM extends Competitors {
  val isMale = true
  val isAdult = false
}

case object JuniorW extends Competitors {
  val isMale = false
  val isAdult = false
}

case object YouthM extends Competitors {
  val isMale = true
  val isAdult = false
}

case object YouthW extends Competitors {
  val isMale = false
  val isAdult = false
}
