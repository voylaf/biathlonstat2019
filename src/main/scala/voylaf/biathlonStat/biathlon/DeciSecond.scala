package voylaf.biathlonStat.biathlon

/*
 * @param i time in deciseconds
 */
//todo: implicit numeric
case class DeciSecond(i: Int) extends Ordered[DeciSecond] {
  override def toString: String = {
    val hour = i / 36000
    val rest1 = i - 36000 * hour
    val min = rest1 / 600
    val rest2 = rest1 - min * 600
    val sec = rest2 / 10
    val decisecond = rest2 - sec * 10
    toStringWith0(hour, min, sec, decisecond)
  }

  def toStringWith0(hour: Int, min: Int, sec: Int, ds: Int) = {
    val hourandmin = if (hour == 0) min.toString
    else {
      if (min < 10) s"$hour:0$min"
      else s"$hour:$min"
    }
    val sec0 = if (sec < 10) s"0$sec" else sec.toString
    s"$hourandmin:$sec0.$ds"
  }

  def compare(that: DeciSecond): Int = this.i compare that.i
}

object DeciSecond {
  def apply(min: Int, sec: Int, decisecond: Int, hour: Int = 0) = {
    val ds = decisecond + 10 * sec + 10 * 60 * min + 10 * 60 * 60 * hour
    new DeciSecond(ds)
  }

  def apply(s: String) = {
    val dissection = "[:.,]?[0-9]+[:.,]?".r
    val cleaning = "[0-9]+".r
    val sections = dissection.findAllIn(s)
    val ints = sections.map { section =>
      cleaning.findFirstIn(section) match {
        case Some(i) => i.toInt
        case None => 0
      }
    }.toList

    val ds = (ints.reverse zip List(1, 10, 600, 36000))
      .map(x => x._1 * x._2).sum
    new DeciSecond(ds)
  }
}