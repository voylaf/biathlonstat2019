package voylaf.biathlonStat.biathlon

case class Race(
                 softVersion: SoftVersion,
                 level: CompetitionLevel,
                 format: CompetitionFormat,
                 //Название проще парсить с сайта, поэтому из пдфки его парсить не будем
                 name: String = System.currentTimeMillis().toString,
                 date: java.util.Date,
                 competitors: Competitors,
                 //                 isMale: Boolean,
                 //                 isAdult: Boolean = true,
                 competitorsResult: List[CompetitorResult]) {

}
