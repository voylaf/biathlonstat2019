package voylaf.biathlonStat.biathlon

class RaceStat(val race: Race) {
  
  val competitorsResultStat: List[CompetitorResultStat] = {
    val competitorsResult = fixBadData(race.competitorsResult)
    val sortedByTimeRun = sortByTimeRun(competitorsResult)
    val withPositions = (sortedByTimeRun zip (1 to sortedByTimeRun.length)).toMap

    race.format match {
      case Sprint => competitorsResult.map { x => 
        val penaltyProne = x.penalty take 1
        val penaltyStandings = x.penalty takeRight 1
        val timeProne = x.timeShooting take 1
        val timeStandings = x.timeShooting takeRight 1
        val positionTimeRun = withPositions.getOrElse(x, -1)
        CompetitorResultStat(x, positionTimeRun, penaltyProne, penaltyStandings, timeProne, timeStandings)
      }
      case _ => competitorsResult.map(CompetitorResultStat(_))
    }
  }
  
  def sortByTimeRun(competitorsResult: List[CompetitorResult]) = competitorsResult.sortBy{x => x.timeRun.map(ds => ds.i).sum}
  
  def fixBadData(competitorsResult: List[CompetitorResult]) = {
    val badData = "Bad data"
    competitorsResult.map { cr =>
      if (cr.surname == badData || cr.name == badData || cr.region == badData) {
        println(cr)
        println("Enter surname, please")
        val surnameR = scala.io.StdIn.readLine()
        println("Enter name, please")
        val nameR = scala.io.StdIn.readLine()
        println("Enter region, please")
        val regionR = scala.io.StdIn.readLine()
        cr.copy(surname = surnameR, name = nameR, region = regionR)
      }
      else cr
    }
  }
  
}