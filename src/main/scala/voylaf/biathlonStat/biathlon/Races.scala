package voylaf.biathlonStat.biathlon

import voylaf.biathlonStat.parsing.PDFParceSprint
import voylaf.biathlonStat.patterns.{PatternSki123, PatternVZh, PatternVZh1}

object Races {
  case class PDFParceVZh1(val path: String, val pdfName: String) extends PatternVZh1 with PDFParceSprint
  case class PDFParceVZh(val path: String, val pdfName: String) extends PatternVZh with PDFParceSprint
  case class PDFParceSki123(val path: String, val pdfName: String) extends PatternSki123 with PDFParceSprint

  val defaultPath = """C:\Development\files\Biathlon Russian Cup\"""

  lazy val pdf20161212sprintwvz1Race = {
    val pdfName = """20161212sprintw_vz1.pdf"""
    PDFParceVZh1(defaultPath, pdfName).race
  }

  lazy val pdf20170305sprintw_vzRace = {
    val pdfName = """20170305sprintw_vz.pdf"""
    PDFParceVZh(defaultPath, pdfName).race
  }

  lazy val pdf20161201sprintm_123Race = {
    val pdfName = """20161201sprintm_123.pdf"""
    PDFParceSki123(defaultPath, pdfName).race
  }

  lazy val pdf20161201sprintw_123Race = {
    val pdfName = """20161201sprintw_123.pdf"""
    PDFParceSki123(defaultPath, pdfName).race
  }

  lazy val pdf20170325sprintw_vzRace = {
    val pdfName = """20170325sprintw_vz.pdf"""
    PDFParceVZh(defaultPath, pdfName).race
  }
}
