package voylaf.biathlonStat.database

import scalikejdbc.{AutoSession, ConnectionPool, DBSession, WrappedResultSet, _}
import voylaf.biathlonStat.biathlon._
import voylaf.biathlonStat.parsing.IO

object DatabaseManagement {
  // http://scalikejdbc.org/
  // initialize JDBC driver & connection pool
  Class.forName("org.postgresql.Driver")
  ConnectionPool.singleton("jdbc:postgresql:BiathlonStat", "scalaApps", "1")
  val con = ConnectionPool.borrow()

  // ad-hoc session provider on the REPL
  implicit val session = AutoSession

  //Don't forget: (implicit session: DBSession) after each def

  /*
   * add competition in competitions, raceStat in sprints/other tables
   */
  //todo: Нужно изменить схему БД и операции с ней, связанные с полом и возрастом, так как я поменял Race
  def addRaceStat(raceStat: RaceStat, competitionId: Int)(implicit session: DBSession) = {
    def penaltySprint(list: List[Int]) = list match {
      case x :: Nil => x
      case _        => throw new Exception(s"Штраф не найден")
    }
    val competitorsResultStat = raceStat.competitorsResultStat
    val format = raceStat.race.format
    format match {
      case Sprint => addSprint(competitorsResultStat, competitionId)
      case _      => throw new Exception(s"$format - неверный формат соревнований")
    }
  }

  def addSprint(competitorsResultStat: List[CompetitorResultStat], competitionId: Long)(implicit session: DBSession) = {
    def penaltySprint(list: List[Int]) = list match {
      case x :: Nil => x
      case _        => throw new Exception(s"Штраф не найден")
    }

    val namesWithId = namesId(competitorsResultStat)
    /* todo: there are several duplicates like "Иванов Иван" x2
     * (key -> value) => (value -> key)
     */
    val values = competitorsResultStat.map { cr =>
      val snKey = (cr.competitorResult.surname, cr.competitorResult.name)
      val result = cr.competitorResult
      val List(timeRun1, timeRun2, timeRun3) = result.timeRun
      val List(timeShootingProne, timeShootingStanding) = result.timeShooting

      Seq(
        'competition_id -> competitionId,
        'biathlete_id -> namesWithId.getOrElse(snKey, -1),
        'penalty_prone -> penaltySprint(cr.penaltyProne),
        'penalty_standings -> penaltySprint(cr.penaltyStandings),
        'time_total -> result.timeTotal.i,
        'time_run1 -> timeRun1.i,
        'time_run2 -> timeRun2.i,
        'time_run3 -> timeRun3.i,
        'time_shooting_prone -> timeShootingProne.i,
        'time_shooting_standing -> timeShootingStanding.i,
        'is_time_shooting_valid -> result.isTimeShootingValid,
        'position_total -> result.position,
        'position_run -> cr.runningPosition,
        'region -> result.region)
    }

    sql"""insert into sprints (competition_id, biathlete_id, penalty_prone, penalty_standings, time_total,
          time_run1, time_run2, time_run3, time_shooting_prone, time_shooting_standing, is_time_shooting_valid,
          position_total, position_run, region)
          values ({competition_id}, {biathlete_id}, {penalty_prone}, {penalty_standings}, {time_total},
          {time_run1}, {time_run2}, {time_run3}, {time_shooting_prone}, {time_shooting_standing}, {is_time_shooting_valid},
          {position_total}, {position_run}, {region})""".batchByName(values: _*).apply[Seq]()
  }

  //todo: подумать над верным запросом: не получать лишние совпадения
  def namesId(list: List[CompetitorResultStat])(implicit session: DBSession): Map[(String, String), Int] = list match {
    case x :: xs => {
      val surnamesAndNames = list.map(cr => (cr.competitorResult.surname, cr.competitorResult.name))
      val (surnames, names) = surnamesAndNames.unzip
      val res = {
        sql"""select biathlete_id, surname, name from biathletes as b
        where b.surname in (${surnames}) AND b.name in (${names})"""
          .map(rs => (rs.int("biathlete_id"), rs.string("surname"), rs.string("name"))).list.apply()
      }

      res.map { x =>
        val (id, surname, name) = x
        (surname, name) -> id
      }.toMap
    }
    case _ => throw new Exception("Пустой список биатлонистов")
  }

  def addCompetition(race: Race)(implicit session: DBSession) = {
    val Race(_, level, format, name, date, competitors, _) = race
    sql"""insert into competitions (level, format, name, date, is_male, is_adult)
        values (${level.toString}, ${format.toString}, $name, $date, ${competitors.isMale}, ${competitors.isAdult})"""
      .updateAndReturnGeneratedKey.apply()
  }

  case class SurnameAndName(surname: Option[String], name: Option[String]) {
    lazy val strings = {
      val option = for {
        s <- this.surname
        n <- this.name
      } yield (s, n)
      option match {
        case Some(x) => x
        case None    => ("", "")
      }
    }
  }

  object SurnameAndName extends SQLSyntaxSupport[SurnameAndName] {
    override val tableName = "names_display"
    def apply(rs: WrappedResultSet) = new SurnameAndName(
      rs.stringOpt("surname"), rs.stringOpt("name"))
  }

  def findNewNames(competitorsResult: List[CompetitorResult])(implicit session: DBSession): Set[(String, String)] = {
    val surnameAndNamesInDB: List[SurnameAndName] =
      sql"""select surname, name from names_display where is_actual = true"""
        .map(rs => SurnameAndName(rs)).list.apply()
    val namesInCompetition = competitorsResult.map(x => (x.surname, x.name))
    namesInCompetition.toSet -- surnameAndNamesInDB.map(x => x.strings).toSet
  }

  def insertNewNames(newNamesSet: Set[(String, String)], isMale: Boolean, autoInsert: Boolean = false)(implicit session: DBSession) = {
    def insertNames(namesSet: Set[(String, String)], isMale: Boolean) = {
      val values = namesSet.map { sn =>
        val (surname, name) = sn
        Seq('surname -> surname, 'name -> name, 'is_male -> isMale, 'isDeleted -> false)
      }.toSeq

      sql"insert into biathletes (surname, name, is_male, is_deleted) values ({surname}, {name}, {is_male}, {isDeleted})"
        .batchByName(values: _*).apply[Seq]()
    }

    val toDB = newNamesSet.filter { sn =>
      val (surname, name) = sn
      autoInsert || IO.isAnswerYes(s"$surname $name нет в базе данных, хотите добавить (1/0)?")
    }

    insertNames(toDB, isMale)

    val other = newNamesSet -- toDB

    insertOtherNames(other)
  }

  def insertOtherNames(other: Set[(Surname, Name)]) = {
    if (!other.isEmpty && IO.isAnswerYes("Желаете ли добавить для оставшихся биатлонистов связь с прежними именами в базе данных (1/0)?")) {
      other.foreach {
        case (surname, name) =>
          val (correctSurname, correctName) = IO.getCorrectName(surname, name)
          val changer = Changer((surname, name), (correctSurname, correctName))
          changeAlias(changer)
      }
    } else other.foreach(x => s"${x._1} ${x._2} - биатлонист не добавлен в базу данных")
  }

  case class IDSurnameName(id: Option[Long], surname: Option[String], name: Option[String]) {
    lazy val value = {
      val option = for {
        id <- this.id
        s <- this.surname
        n <- this.name
      } yield (id, s, n)
      option match {
        case Some(x) => x
        case None    => (-1L, "", "")
      }
    }
  }

  object IDSurnameName extends SQLSyntaxSupport[IDSurnameName] {
    def apply(rs: WrappedResultSet) = new IDSurnameName(
      rs.longOpt("biathlete_id"), rs.stringOpt("surname"), rs.stringOpt("name"))
  }

  def addAliases()(implicit session: DBSession) = {
    val fromDB =
      sql"""select b.biathlete_id, b.surname, b.name
      from biathletes as b left join names_display as nd on b.biathlete_id = nd.biathlete_id
      where nd.name isnull"""
      .map(rs => IDSurnameName(rs)).list.apply()

    val aliases = fromDB.map {
      case x =>
        val (id, surname, name) = x.value
        id -> (surname, name)
    }.toMap

    addAliasesToDB(aliases)
  }

  /*
   * Для всех псевдонимов гарантировано, что в БД нет других псевдонимов для соответствующих биатлонистов
   */
  def addAliasesToDB(aliases: Map[Long, (Surname, Name)], isActual: Boolean = true)(implicit session: DBSession) = {
    val values = aliases.toSeq.map {
      case (id, (surname, name)) =>
        val nameShow = s"$name $surname"
        Seq('biathlete_id -> id, 'nameShow -> nameShow, 'isActual -> isActual, 'surname -> surname, 'name -> name)
    }
    sql"insert into names_display (biathlete_id, name_show, is_actual, surname, name) values ({biathlete_id}, {nameShow}, {isActual}, {surname}, {name})"
      .batchByName(values: _*).apply[Seq]()
  }

  type Name = String
  type Surname = String
  case class Changer(incorrect: (Surname, Name), correct: (Surname, Name))
  def changeAlias(changer: Changer)(implicit session: DBSession) = {
    val id = aliasCollisionHandler(findBiatleteByAlias(changer.incorrect))
    val (cSurname, cName) = changer.correct
    //Убираем пометку актуальности
    sql"update names_display set is_actual = false where biathlete_id = ${id} and is_actual = true".update().apply()
    //Добавляем верный вариант
    addAliasesToDB(Map(id -> changer.correct))
  }

  def findBiatleteByAlias(alias: (Surname, Name))(implicit session: DBSession): List[Long] = {
    val (surname, name) = alias
    val ids: List[Long] =
      sql"select biathlete_id from names_display where name = ${name} and surname = ${surname} and is_actual = true"
        .map(rs => rs.long("biathlete_id"))
        .list
        .apply()
    ids
  }

  def aliasCollisionHandler(ids: List[Long]): Long = ids match {
    case i :: Nil => i
    case Nil      => throw new Exception("Псевдоним не найден в базе")
    //todo: Разрешить коллизию: несколько биатлонистов имеют одинаковые имена
    case _        => throw new Exception(s"Несколько биатлонистов имеют одинаковые имена: $ids")
  }

  def mergeBiathletes(changer: Changer)(implicit session: DBSession) = {
    /*def updateBiathleteId(incorrectId: Long, correctId: Long, tableNames: String*) = {
      tableNames.foreach { tableName =>
        sql"update ${tableName} set biathlete_id = ${correctId} where biathlete_id = ${incorrectId}".update().apply()
      }
    }*/

    val List(incorrectId, correctId) = List(changer.incorrect, changer.correct).map(alias => aliasCollisionHandler(findBiatleteByAlias(alias)))
    //Убираем пометку актуальности
    sql"update names_display set is_actual = false where biathlete_id = ${incorrectId} and is_actual = true".update().apply()
    //Обновляем id на верный для всех таблиц
    sql"update names_display set biathlete_id = ${correctId} where biathlete_id = ${incorrectId}".update().apply()
    sql"update sprints set biathlete_id = ${correctId} where biathlete_id = ${incorrectId}".update().apply()
    //Ставим пометку удаления
    sql"update biathletes set is_deleted = true where biathlete_id = ${incorrectId}".update().apply()
  }
}
