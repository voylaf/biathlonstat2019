package voylaf.biathlonStat.fpParsing

import voylaf.biathlonStat.biathlon._

import scala.language.higherKinds

//import Sliceable._
import voylaf.biathlonStat.fpParsing.SliceableTypes._

object AnalysisParser {

  def parse[Parser[+ _]](P: Parsers[Parser]) = {
    import P.{string => _, _}
    implicit def tok(s: String) = token(P.string(s))
  }

  def parseSoft(implicit P: Parsers[Parser]): Parser[SoftVersion] = {
    import P._
    val soft123 = thru("Ski") | thru("ski")
    val softZhizhin = thru("Zhizhin")

    scope("soft")(
      softZhizhin.as(VZhizhin) | soft123.as(Ski123)
    )
  }

  def timeB(implicit P: Parsers[Parser]): Parser[String] = {
    import P._
    token("([+]?)([1-9]:)?([0-5]?[0-9]:)?[0-5]?[0-9]\\.[0-9]".r) label "time or gap"
  }

  def ordNumber(implicit P: Parsers[Parser]): Parser[String] = {
    import P._
    surround("\\s*".r, "\\s+".r)("=?[0-9]{1,3}".r) label "ordered number"
  }

  def surname(implicit P: Parsers[Parser]): Parser[String] = {
    import P._
    token("[-А-ЯЁ]*".r) label "surname"
  }

  def name(implicit P: Parsers[Parser]): Parser[String] = {
    import P._
    token("[А-ЯЁ][-а-яА-ЯЁё]*".r) label "name"
  }

  def region(implicit P: Parsers[Parser]): Parser[String] = {
    import P._
    token("[А-Я][-а-яА-Яё ]*".r) label "region"
  }

  def parseDate(implicit P: Parsers[Parser]): Parser[java.util.Date] = {
    import P._

    val days = "[0-9]{1,2}".r
    val month = "[а-яА-Я]{3}".r
    val year = "[0-9]{4}".r

    val date3 = rec(token(days) ** token(month) ** token(year))

    scope("date in format dd MMM yyyy") {
      date3.map { case ((d, m), y) => Handlers.string2Date(d, m, y) }
    }
  }

  //todo: other competitors like Headers
  def parseCompetitors(implicit P: Parsers[Parser]): Parser[Competitors] = {
    import P._
    val men = upperOrLowerCases("мужчины")
    val women = upperOrLowerCases("женщины")

    scope("men or women etc") {
      men.as(Men) |
        women.as(Women)
    }
  }

  def nameSurnameRegion(implicit P: Parsers[Parser]): Parser[((String, String), String)] = {
    import P._
    val empty = "".r
    (surname ** name ** region) |
      (surname ** name ** empty) |
      (empty ** empty ** region) |
      (empty ** empty ** empty)
  }

  //todo: try ** and shapeless instead flatMap

  //  def parseMainString(P: Parsers[Parser])(shootingCount: Int): Parser[LaconicBiathleteResult] = {
  //    import P._
  //    for {
  //      _ <- ordNumber
  //      _ <- ordNumber
  //      surn <- surname
  //      na <- name
  //      reg <- region
  //      penalty <- listOfN(shootingCount, "[0-9]".r <* " ")
  //      sum <- "[0-9]{1,2}".r <* " "
  //      _ <- timeB
  //      _ <- timeB
  //    } yield LaconicBiathleteResult(surn, na, reg, penalty, sum)
  //  }

  def parseLaconicBiathleteResult(shootingCount: Int)(implicit P: Parsers[Parser]): Parser[LaconicBiathleteResult] = {
    import P._
    /*copy-paste from
    https://github.com/milessabin/shapeless/blob/master/examples/src/main/scala/shapeless/examples/flatten.scala
     */
    import shapeless._
    import ops.tuple.FlatMapper
    //import syntax.std.tuple._
    trait LowPriorityFlatten extends Poly1 {
      implicit def default[T] = at[T](Tuple1(_))
    }
    object flatten extends LowPriorityFlatten {
      implicit def caseTuple[P <: Product](implicit lfm: Lazy[FlatMapper[P, flatten.type]]) =
        at[P](lfm.value(_))
    }

    val parser = ordNumber *> ordNumber *> nameSurnameRegion **
      listOfN(shootingCount, token("[0-9]".r)) ** token("[0-9]{1,2}".r) ** timeB <* many(timeB)

    parser.map(flatten).map {
      case (surname, name, region, penalty, penaltySum, sumTime) =>
        LaconicBiathleteResult(surname, name, region, penalty, penaltySum, sumTime)
    }
  }

  //todo: stackoverflow prevention
  def recEx[A](p: Parser[A])(implicit P: Parsers[Parser]): Parser[A] = {
    import P._
    rec(p)
  }

  def parseHeader(implicit P: Parsers[Parser]): Parser[Header] = {
    import P._, Header._
    scope("header")(token(stringToHeaders
      .map { case (string, header) => string.r.as(header) }
      .reduce(_ | _)))
  }

  def parseHeaders(implicit P: Parsers[Parser]): Parser[List[Header]] = {
    import P._
    scope("headers")(rec(many1(token(parseHeader))))
  }

  def parseCell3(implicit P: Parsers[Parser]): Parser[Cell3] = {
    import P._
    scope("cell with 3 values")(timeB ** timeB ** ordNumber).map(Cell3.fromRaw)
  }

  def parseCell1(implicit P: Parsers[Parser]): Parser[Cell1] = {
    import P._
    scope("cell with 1 value")(timeB.map(Cell1))
  }

  def parseCell(implicit P: Parsers[Parser]): Parser[Cell] = {
    import P._
    scope("cell")(parseCell3 | parseCell1)
  }

  def parseRow(implicit P: Parsers[Parser]): Parser[Row] = {
    import P._
    scope("row")(parseHeader ** many1(parseCell))
      .map { case (header, cells) => Row(header, cells) }
  }

  def parseTable(implicit P: Parsers[Parser]): Parser[Table] = {
    import P._
    scope("table")(many(parseRow).map(Table))
  }

  def parseVerboseBiathleteResult(shootingCount: Int)(implicit P: Parsers[Parser]): Parser[VerboseBiathleteResult] = {
    import P._
    scope("verbose biathlete result")(
      parseLaconicBiathleteResult(shootingCount) ** parseTable map { case (br, t) => VerboseBiathleteResult(br, t) })
  }

  def parseResults(shootingCount: Int)(implicit P: Parsers[Parser]): Parser[List[VerboseBiathleteResult]] = {
    import P._
    parseVBRS(shootingCount)
  }

  def parseBlocks(shootingCount: Int)(implicit P: Parsers[Parser]): Parser[List[Block]] = {
    import P._
    scope("list of vbr")(
      many1(parseBlock(shootingCount)))
  }

  def parseVBRS(shootingCount: Int)(implicit P: Parsers[Parser]): Parser[List[VerboseBiathleteResult]] = {
    import P._
    scope("list of vbr")(
      (parseBlocks(shootingCount) ** parseLastBlock(shootingCount))
        .map { case (bs, last) =>
          (bs ::: last :: Nil).reduce(_ merge _).vbrs
        })
  }

  //  def parseResultsDebug(shootingCount: Int)(implicit P: Parsers[Parser]): Parser[(List[VerboseBiathleteResult], String)] = {
  //    import P._
  //    many1(parseVerboseBiathleteResult(shootingCount)) ** token(".*".r)
  //  }

  def parseBlock(shootingCount: Int)(implicit P: Parsers[Parser]): Parser[Block] = {
    import P._
    scope("block of vbrs")(rec(
      many(parseRow) ** many1(parseVerboseBiathleteResult(shootingCount)))
      .map { case (rs, vbrs) => Block(rs, vbrs) })
  }

  def parseLastBlock(shootingCount: Int)(implicit P: Parsers[Parser]): Parser[Block] = {
    import P._
    scope("the last block")(rec(
      many1(parseRow) ** many(parseVerboseBiathleteResult(shootingCount))) | succeed(Nil, Nil)
      map { case (rs, vbrs) => Block(rs, vbrs) })
  }

  def parseCompetitionFormat(implicit P: Parsers[Parser]): Parser[CompetitionFormat] = {
    import P._
    //todo: добавить другие форматы
    val sprint = upperOrLowerCases("СПРИНТ")
    val individual = token(upperOrLowerCases("ГОНКА")) <* token(digits) <* token("КМ")
    val pursuit = upperOrLowerCases("ПЕРСЬЮТ")

    scope("race format")(
      individual.as(Individual) |
        sprint.as(Sprint) |
        pursuit.as(Pursuit))
  }

  def parseResults(implicit P: Parsers[Parser]): Parser[(CompetitionFormat, List[BiathleteResult])] = {
    import P._
    scope("parse results")(
      for {
        cf <- parseCompetitionFormat
        headers <- parseHeaders
        vbrs <- parseVBRS(cf.shootings)
        fbrs <- parseFBRS(vbrs)(headers)
        brs <- parseBiathleteResults(cf)(fbrs)
      } yield (cf, brs)
    )
  }

  //todo: rewrite for the more useful tests

  def parseFBRS(vbrs: List[VerboseBiathleteResult])(headers: List[Header])(implicit P: Parsers[Parser]): Parser[List[FullBiathleteResult]] = {
    import P._
    scope("parse full biathlete results")(
      succeed(vbrs.map(vbr => FullBiathleteResult.fromVBR(vbr)(headers)))
    )
  }

  def parseBiathleteResults(cf: CompetitionFormat)(fbrs: List[FullBiathleteResult])(implicit P: Parsers[Parser]): Parser[List[BiathleteResult]] = {
    import P._
    scope("parse summary biathlete results")(
      succeed(fbrs.map(fbr => BiathleteResult.get(cf)(fbr)))
    )
  }

}


