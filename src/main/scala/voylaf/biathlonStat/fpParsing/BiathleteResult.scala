package voylaf.biathlonStat.fpParsing

import voylaf.biathlonStat.biathlon.{CompetitionFormat, DeciSecond}

import scala.util.Try

trait BiathleteResult {
  def surname: String

  def name: String

  def region: String

  def summaryTime: DeciSecond

  def penaltySum: Int

  def penaltyProne: Option[List[Int]]

  def penaltyStanding: Option[List[Int]]

  def lapTimes: Option[List[DeciSecond]]

  def speedTimes: Option[List[DeciSecond]]

  def shootingProneTimes: Option[List[DeciSecond]]

  def shootingStandingTimes: Option[List[DeciSecond]]

  def firingLineProneTimes: Option[List[DeciSecond]]

  def firingLineStandingTimes: Option[List[DeciSecond]]
}

object BiathleteResult {
  type Cells = Map[Set[Header], Cell]

  val laps: Vector[Header] = Vector(Lap1, Lap2, Lap3, Lap4, Lap5)

  def get(cf: CompetitionFormat)(fbr: FullBiathleteResult): BiathleteResult = {
    val LaconicBiathleteResult(surname1, name1, region1, penalty, penaltySum1, summaryTime1) = fbr.laconic
    val table = fbr.fullTable.cells
    new BiathleteResult {
      val surname: String = surname1

      val name: String = name1

      val region: String = region1

      val summaryTime: DeciSecond = DeciSecond(summaryTime1)

      val penaltySum: Int = penaltySum1.toInt

      val penaltyProne: Option[List[Int]] = Try(cf.shootingProne.map(i => penalty(i - 1).toInt)).toOption

      val penaltyStanding: Option[List[Int]] = Try(cf.shootingStanding.map(i => penalty(i - 1).toInt)).toOption

      val lapTimes: Option[List[DeciSecond]] = getLapTimes((1 to cf.lapsCount).toList)(LapTime)(table)

      val speedTimes: Option[List[DeciSecond]] = getLapTimes((1 to cf.lapsCount).toList)(SpeedTime)(table)

      val shootingProneTimes: Option[List[DeciSecond]] = getLapTimes(cf.shootingProne)(ShootingTime)(table)

      val shootingStandingTimes: Option[List[DeciSecond]] = getLapTimes(cf.shootingStanding)(ShootingTime)(table)

      val firingLineProneTimes: Option[List[DeciSecond]] = getLapTimes(cf.shootingProne)(FiringLineTime)(table)

      val firingLineStandingTimes: Option[List[DeciSecond]] = getLapTimes(cf.shootingStanding)(FiringLineTime)(table)

      override def toString: String = {
        def optToString(opt: Option[List[_]])(implicit sep: String) =
          opt.map(_.mkString(sep)).mkString

        implicit val sep: String = "|"

        s"""$surname $name, выступающий за регион $region, пробежал за $summaryTime1,
           |допустив ${optToString(penaltyProne)("+")} промахов на стрельбе лежа
           |и ${optToString(penaltyStanding)("+")} промахов на стрельбе стоя
           |время кругов: ${optToString(lapTimes)}
           |скорость по дистанции: ${optToString(speedTimes)}
           |время стрельбы лежа: ${optToString(shootingProneTimes)}
           |время стрельбы стоя: ${optToString(shootingStandingTimes)}
           |время на рубежах лежа: ${optToString(firingLineProneTimes)}
           |время рубежах стоя: ${optToString(firingLineStandingTimes)}
           |""".stripMargin
      }
    }
  }

  def getLapTimes(lapsCount: List[Int])(timeHeader: Header)(cells: Cells): Option[List[DeciSecond]] = {
    val keys = Try(lapsCount.map(i => Set(laps(i - 1), timeHeader))).toOption
    keys.map(ls => ls.flatMap(hs => cells.get(hs).map(_.timeDS)))
  }


}