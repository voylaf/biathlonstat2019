package voylaf.biathlonStat.fpParsing

import voylaf.biathlonStat.biathlon.{CompetitionFormat, Competitors}

case class Competition(
                        //softVersion: SoftVersion,
                        //level: CompetitionLevel,
                        format: CompetitionFormat,
                        //Название проще парсить с сайта, поэтому из пдфки его парсить не будем
                        date: java.util.Date,
                        competitorsLevel: Competitors,
                        competitorsResult: List[VerboseBiathleteResult]
                      )
