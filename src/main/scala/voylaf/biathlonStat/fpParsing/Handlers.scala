package voylaf.biathlonStat.fpParsing

object Handlers {

  def string2Date(s: (String, String, String)): java.util.Date = {
    import java.text.SimpleDateFormat
    lazy val dateformat: SimpleDateFormat = new SimpleDateFormat("ddMMyyyy")
    val ru2MonthC: Map[String, String] = Map(
      "ЯНВ" -> "01",
      "ФЕВ" -> "02",
      "МАР" -> "03",
      "АПР" -> "04",
      "МАЙ" -> "05",
      "ИЮН" -> "06",
      "ИЮЛ" -> "07",
      "АВГ" -> "08",
      "СЕН" -> "09",
      "ОКТ" -> "10",
      "НОЯ" -> "11",
      "ДЕК" -> "12",
    ) withDefaultValue "06"

    val ru2Month = ru2MonthC.map { case (k, v) => (k.toLowerCase, v) } ++ ru2MonthC

    val (dd, mm, y) = s
    val d = if (dd.length == 1) "0" + dd else dd
    val m = ru2Month(mm)

    dateformat.parse(d + m + y)
  }
}
