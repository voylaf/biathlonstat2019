package voylaf.biathlonStat.fpParsing

sealed trait Header {}
/*
Место
Ст. №
Фамилия
Имя
Регион
Л
С
Сум
Время
Отст.
Описание
Круг 1
Круг 2
Круг 3
Круг 4
Круг 5
Итог
Промахи
Круг
Трасса
Стрельба
Огневой рубеж + Штраф. круг
Общее
М-о
Огневой рубеж
Штраф
Общее время
Время на стрельбище
Время круга
Время на трассе
 */

object Header {
  //without relay races
  //very important: priority
  val stringToHeaders: List[(String, Header)] = List(
    "Круг 1" -> Lap1,
    "Круг 2" -> Lap2,
    "Круг 3" -> Lap3,
    "Круг 4" -> Lap4,
    "Круг 5" -> Lap5,
    "Трасса" -> SpeedTime,
    "Общее время" -> SummaryTime,
    "Стрельба" -> ShootingTime,
    "Итог" -> SummaryTime,
    "Общее" -> SummaryTime,
    "Огневой рубеж" -> FiringLineTime,
    "Время на рубеже" -> FiringLineTime,
    "Время круга" -> LapTime,
    "Время на трассе" -> SpeedTime,
    "Штраф" -> PenaltyTime,
    "Круг" -> LapTime,
    "Время на стрельбище" -> FiringLineTime
  )
}

case object Lap1 extends Header {}

case object Lap2 extends Header {}

case object Lap3 extends Header {}

case object Lap4 extends Header {}

case object Lap5 extends Header {}

//Трасса Стрельба Огневой рубеж Штраф Круг Общее
case object SpeedTime extends Header {}

case object ShootingTime extends Header {}

case object SummaryTime extends Header {}

case object FiringLineTime extends Header {}

case object PenaltyTime extends Header {}

case object LapTime extends Header {}