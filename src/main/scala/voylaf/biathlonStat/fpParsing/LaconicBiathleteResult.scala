package voylaf.biathlonStat.fpParsing

case class LaconicBiathleteResult(surname: String, name: String, region: String, penalty: List[String], penaltySum: String, sumTime: String)

case class VerboseBiathleteResult(laconic: LaconicBiathleteResult,
                                  table: Table)

case class FullBiathleteResult(laconic: LaconicBiathleteResult,
                               fullTable: FullTable)

object FullBiathleteResult {
  def fromVBR(vbr: VerboseBiathleteResult)(headers: List[Header]): FullBiathleteResult = {
    val cellWithHeaders = vbr.table.rows.flatMap { row =>
      val hs =
        if (headers.length == row.cells.length) headers
        //todo: print to log
        //it's a potential bug
        else if (headers.headOption.contains(LapTime)) headers.drop(1)
        else headers take row.cells.length
      (hs zip row.cells)
        .map { case (header, cell) => CellWithHeaders(Set(header, row.rowHeader), cell) }
    }

    FullBiathleteResult(
      vbr.laconic,
      FullTable.fromCellWithHeaders(cellWithHeaders))
  }

}
