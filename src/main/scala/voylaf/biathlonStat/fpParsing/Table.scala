package voylaf.biathlonStat.fpParsing

import voylaf.biathlonStat.biathlon.DeciSecond

case class Table(rows: List[Row])

case class CellWithHeaders(headers: Set[Header], cell: Cell)

case class FullTable(cells: Map[Set[Header], Cell])

object FullTable {
  def fromCellWithHeaders(cellsWithHeaders: List[CellWithHeaders]): FullTable =
    new FullTable(cellsWithHeaders.groupBy(_.headers).mapValues(_.head.cell))
}

trait Cell {
  def timeDS: DeciSecond
}

case class Cell1(time: String) extends Cell {
  val timeDS = DeciSecond(time)
}

case class Cell3(time: String, diff: String, number: String) extends Cell {
  val timeDS = DeciSecond(time)
}

object Cell3 {
  def fromRaw(xs: ((String, String), String)): Cell3 =
    new Cell3(xs._1._1, xs._1._2, xs._2)

  //todo: eliminate exception
  def fromList(xs: List[String]): Cell3 = xs match {
    case x :: y :: z :: Nil => new Cell3(x, y, z)
    case _ => throw new Exception("!3 args")
  }
}

case class Row(rowHeader: Header, cells: List[Cell])

case class Block(rows: List[Row], vbrs: List[VerboseBiathleteResult]) {
  def merge(that: Block): Block = that.rows match {
    case Nil => Block(Nil, this.vbrs ::: that.vbrs)
    case rs =>
      val VerboseBiathleteResult(_, Table(rows1)) = vbrs.last
      val middle = vbrs.last.copy(table = Table(rows1 ::: rs))
      Block(Nil, vbrs.init ::: (middle :: that.vbrs))
  }

  override def toString: String = {
    s"""the block begins
       |rows = $rows
       |${vbrs.foldLeft("")(_ + _ + "\n")}
       |the block ends
       |""".stripMargin
  }
}
