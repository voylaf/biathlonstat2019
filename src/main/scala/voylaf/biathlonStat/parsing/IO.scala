package voylaf.biathlonStat.parsing

import java.io.FileWriter

object IO {
  def isAnswerYes(s: String) = {
    println(s)
    val char = scala.io.StdIn.readChar()
    char match {
      case 'y' => true
      case 'д' => true
      case '1' => true
      case _ => false
    }
  }

  def isMale: Boolean = {
    println("Выступают мужчины (m/м) или женщины (w/ж)?")
    val char = scala.io.StdIn.readChar()
    char match {
      case 'w' => false
      case 'm' => true
      case 'ж' => false
      case 'м' => true
      case _ => isMale
    }
  }

  def getCompetitionName(pdfName: String) = {
    println(pdfName)
    println("Введите название соревнования")
    scala.io.StdIn.readLine()
  }

  def allPdfInDir(dirPath: String): List[String] = {
    import java.io.File
    val dir = new File(dirPath)
    dir.listFiles.filterNot(_.isDirectory()).map(x => x.getName).toList
  }

  def getCorrectName(surname: String, name: String) = {
    println(s"$name $surname - это новое имя для:")
    println("Введите фамилию")
    val correctSurname = scala.io.StdIn.readLine()
    println("Введите имя")
    val correctName = scala.io.StdIn.readLine()
    (correctSurname, correctName)
  }

  class Params(dir1: String, val autoInsert: Boolean) {
    val dir = if (dir1.endsWith("""\""")) dir1 else dir1 + """\"""
  }

  def getParams = {
    println("Папка с пдф для добавления в базу, полный адрес")
    val dir = scala.io.StdIn.readLine()
    new Params(dir, isAnswerYes("Автоматически добавлять биатлонистов в базу (1/0)?"))
  }

  def writeToFile(filename: String = """test.txt""", dir: String = """C:\Development\files\biathlonStat\""")
                 (strings: String): Unit = {
    val file = dir + filename
    val writer = new FileWriter(file)
    val str = strings mkString "\n"
    try {
      writer.append(str).append("\n")
    }
    finally {
      writer.close()
    }
  }
}
