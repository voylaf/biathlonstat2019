package voylaf.biathlonStat.parsing

import voylaf.biathlonStat.biathlon.RaceStat
import voylaf.biathlonStat.database.DatabaseManagement

object Main extends App {

  import voylaf.biathlonStat.database.DatabaseManagement._

  val params = IO.getParams
  val pdfs = IO.allPdfInDir(params.dir)

  class A(val pdfName: String, val path: String) extends PDFParceSprint

  //  def raceResults() =
  //    (p: String) => {
  //      val a = new A(p, params.dir)
  //      val race = a.race
  //      val raceStat = new RaceStat(race)
  //      val id = DatabaseManagement.addCompetition(race).toInt
  //
  //      val newNames = DatabaseManagement.findNewNames(raceStat.competitorsResultStat.map(_.competitorResult))
  //      DatabaseManagement.insertNewNames(newNames, race.competitors.isMale, params.autoInsert)
  //
  //      val namesIds = namesId(raceStat.competitorsResultStat)
  //
  //      DatabaseManagement.addRaceStat(raceStat, id)
  //      DatabaseManagement.addAliases()
  //    }

  def test2Files =
    (p: String) => {
      val pdf = new PDF(params.dir + """\""", p)
      IO.writeToFile("test" + p + ".txt", """C:\Development\files\biathlonStat\box\""")(pdf.result)
    }

  pdfs.foreach { p =>
    test2Files(p)
  }

}
