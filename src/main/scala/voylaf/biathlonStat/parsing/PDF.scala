package voylaf.biathlonStat.parsing

import java.io._

class PDF(val folder: String, val fileName: String) {

  import org.apache.pdfbox.pdmodel.PDDocument
  import org.apache.pdfbox.text.PDFTextStripper
  //Loading an existing document

  val file = new File(folder + fileName)
  val document: PDDocument = PDDocument.load(file)

  //Instantiate PDFTextStripper class
  val pdfStripper = new PDFTextStripper

  //Retrieving text from PDF document
  val result: String = pdfStripper.getText(document).replaceAll("\\s+", " ")

  //Closing the document
  document.close()
}