package voylaf.biathlonStat.parsing

import voylaf.biathlonStat.biathlon._
import voylaf.biathlonStat.patterns.{PatternSki123, PatternVZh, PatternVZh1}

trait PDFParceSprint {
  val pdfName: String
  val path: String
  val pdf = new PDF(path, pdfName)

  val fileNameEMessage = s"$pdfName - Неверный формат названия файла: между '_' и '=' должен быть предполагаемый паттерн. Например: 20161201sprintw_123=62"

  /*
   * Как-то кривовато вышло с наследованием трейта.
   * Может быть, есть вариант попрямее?
   */
  val auxObject = {
    val patpat = "_.*=".r
    val pat = patpat.findFirstIn(pdfName) match {
      case Some(str) => str
      case None      => throw new Exception(fileNameEMessage)
    }
    pat match {
      case "_123=" =>
        //class A extends PatternSki123; new A
        new PatternSki123 {}
      case "_vz=" =>
        //class A extends PatternVZh; new A
        new PatternVZh {}
      case "_vz1=" =>
        //class A extends PatternVZh1; new A
        new PatternVZh1 {}
      case _ => throw new Exception(s"$pdfName - Паттерн $pat не найден")
    }
  }

  def competitorsResults(debug: Boolean = false) = {
    val cr = auxObject.getCompetitorsResults(pdf, debug)
    val pat = "=[0-9]+".r
    val correctNumbers = pat.findFirstIn(pdfName) match {
      case Some(n) => (n drop 1).toInt
      case None    => throw new Exception(fileNameEMessage)
    }
    if (cr.size == correctNumbers) cr
    else {
      cr.foreach(println(_))
      throw new Exception(s"$pdfName - Количество завершивших дистанцию биатлонистов не соответствует ожидаемому: ${cr.size} != $correctNumbers")
    }
  }

  def getDate(pdfName: String) = {
    import java.text.SimpleDateFormat
    lazy val dateformat: SimpleDateFormat = new SimpleDateFormat("yyyyMMdd")
    dateformat.parse(pdfName take 8)
  }

  lazy val date = getDate(pdfName)

  lazy val race = Race(
    //Этот код планируется удалить, поэтому для совместимости выставил по дефолту Жижина и мужчин
    softVersion = VZhizhin,
    level = RussianCup,
    format = Sprint,
    name = competitionName,
    date = date,
    competitors = Men,
    competitorsResults())

  lazy val competitionName: String = IO.getCompetitionName(pdfName)

  lazy val isMale: Boolean = IO.isMale
}
