package voylaf.biathlonStat.parsing

import voylaf.biathlonStat.biathlon.{CompetitorResultStat2, RaceStat}
import voylaf.biathlonStat.fpParsing.Handlers

import scala.util.matching.Regex

object ParseRace {

  def parseRace(p: Parser)(string: String): RaceStat = p.parseRace(string)

  import Regexs._

  def parseDate: String => Either[String, java.util.Date] = s => {
    val date3: Regex = daysP ~ monthP ~ yearP

    val res = date3.findFirstIn(s)
    res.map {
      case date3(d, m, y) => Right(Handlers.string2Date(d, m, y))
    }.getOrElse(Left(s"date not found in $s"))
  }

  def parseMainString(s: String)(shootings: Int): Either[String, CompetitorResultStat2] = {
    val regex: Regex = ordNumberP ~ ordNumberP ~ surnameP ~ nameP ~ regionP ~ penaltyN(shootings + 1) ~ timeB ~ timeB.?
    val mainString = regex.findFirstIn(s)
    mainString.map {
      case regex(startPos, finalPos, surname, name, region, penalty, time, _) =>
        Right(CompetitorResultStat2(startPos, finalPos, surname, name, region, penalty, time))
      case _ => Left(s"fail main string when pattern matching: $s")
    }.getOrElse(Left(s"fail main string: $s"))
  }
}

