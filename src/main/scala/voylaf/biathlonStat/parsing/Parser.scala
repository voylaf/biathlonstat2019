package voylaf.biathlonStat.parsing

import voylaf.biathlonStat.biathlon.RaceStat

trait Parser {
  def parseRace(string: String): RaceStat
}
