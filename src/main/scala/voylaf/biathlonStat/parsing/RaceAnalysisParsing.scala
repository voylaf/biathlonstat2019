package voylaf.biathlonStat.parsing

import voylaf.biathlonStat.biathlon.CompetitorResult

trait RaceAnalysisParsing {
  def oneNumber = """([0-9]\n)"""
  def positionNumber = """[=]?[0-9]+\n"""
  def sectionTime = """[0-9]{0,2}[:]?[0-9]+[.][0-9]\n"""
  def sectionTimeGap = ("""[+]?""" + sectionTime)
  def nameOrRegion = """([-А-Яа-яЁё ]+\n)?"""
  def otherString = """[-А-Яа-яЁё0-9 ]+\n"""

  def blockHead = s"""($positionNumber)$positionNumber$nameOrRegion$nameOrRegion$oneNumber$oneNumber$positionNumber"""

  def fixInvalid(list: List[String]): List[String]

  def dnlc(s: String) = {
    if (!s.isEmpty) s dropRight 1 else s
  }

  def surnameFormat(s: String) = {
    val start = s take 1
    val end = s drop 1
    start + end.toLowerCase()
  }

  //todo: rewrite
  def fixInvalidStrings(list: List[String], invalidStringPattern: String, leftPattern: String, rightPattern: String) = {
    val s = list.mkString("\n")
    val invalidStrings = invalidStringPattern.r.findAllIn(s).toList
    val fixedStrings = invalidStrings.map { is =>
      val left = leftPattern.r.findAllIn(is).mkString("")
      val right = rightPattern.r.findAllIn(is).mkString("")
      //drop "\n"
      (is dropRight 1, List(left.replace(" ", ""), right dropRight 1))
    }.toMap
    list.map(string => fixedStrings.getOrElse(string, List(string))).flatten
  }

  /*
   * "КОРНЕВ Алексей" -> ("КОРНЕВ", "Алексей")
   * todo: complicated names
   */
  def separateSurnameAndName(surnameAndName: String) = {
    val n = surnameAndName.split(" ")
    if (n.length == 2) (n(0), n(1))
    else throw new Exception(s"surname & name bug for $surnameAndName")
  }

  def stringPreparation(pdf: PDF) = {
    //    val withoutEmptyStrings: List[String] = pdf.result.filter(s => s != " ")
    //    val fixedInvalid = fixInvalid(withoutEmptyStrings)
    //    fixedInvalid.mkString("\n")
    pdf.result
  }

  def getCompetitorsResults(pdf: PDF, debug: Boolean = false): List[CompetitorResult]
}
