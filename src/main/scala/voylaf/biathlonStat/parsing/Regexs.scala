package voylaf.biathlonStat.parsing

import scala.language.implicitConversions
import scala.util.matching.Regex

object Regexs {

  implicit class MyReg(s: String) {
    def ~(that: String): String = (s, that) match {
      case ("", _) => that
      case (_, "") => s
      case _ => s ++ ws ++ that
    }

    def ? : String = s + "?"

    def surround(begin: String, end: String): String = begin ++ s ++ end

    def surround(sur: String): String = surround(sur, sur)

    def group: String = s.surround("(", ")")
  }

  implicit def token(s: String): Regex = s.r

  def ws = """\s+"""

  def allP = """.*"""

  def ordNumberP = """(=?[0-9]{1,3})"""

  def surnameP = """([-А-ЯЁ]*)"""

  def nameP = """([А-Я][-а-яА-Яё]*)"""

  def regionP = """([А-Я][-а-яА-Я ]*)"""

  def penalty1 = """[0-9]{1,2}"""

  def penaltyN: Int => String = (n: Int) => List.fill(n)(penalty1).foldLeft("")(_ ~ _).surround("(", ")")

  def daysP = "([0-9]{1,2})"

  def monthP = "([а-яА-Я]{3})"

  def yearP = "([0-9]{4})"

  def timeB = "(([+]?)([1-9]:)?([0-5]?[0-9]:)?[0-5]?[0-9]\\.[0-9])"
}
