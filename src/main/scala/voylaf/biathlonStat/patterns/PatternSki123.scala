package voylaf.biathlonStat.patterns

import voylaf.biathlonStat.biathlon.{CompetitorResult, DeciSecond}
import voylaf.biathlonStat.parsing.{PDF, RaceAnalysisParsing}

trait PatternSki123 extends RaceAnalysisParsing {
  def lap = s"""($sectionTime)$sectionTimeGap$positionNumber"""
  def laps2 = lap * 2
  def laps3 = lap * 3
  def optGap = s"""($sectionTimeGap)?"""
  def optTable = """[-А-Яа-яЁёa-zA-Z0-9№.():\n ]+?"""
  def nameOrRegionInvalid = """[-А-Яа-яЁё ]+[0-9]\n"""
  def blockTimes = s"""$sectionTime$optGap$optTable$otherString$laps3$optTable$otherString$laps3$optTable$otherString$laps2$optTable$otherString$laps3"""
  val blockPattern = (blockHead + blockTimes).r

  def isTimeShootingValid = false

  /*
   * fix strings as "Томская область 3",
   * where "Томская область" is region
   * and "3" is penalty
   * to "Томская область\n3"
   */

  def fixInvalid(list: List[String]): List[String] = {
    fixInvalidStrings(list, nameOrRegionInvalid, """[-А-Яа-яЁё ]+""", """[0-9]+\n""")
  }

  def createCompetitorsResults(string: String) = {
    {for {
      blockPattern(position, surnameAndName, region, penaltyProne, penaltyStanding, _, _, _, timeTotal,
        _, _, _, timeShootingProne, timeShootingStanding, timeRun1, timeRun2, timeRun3) <- blockPattern.findAllIn(string)
      (surname, name) = separateSurnameAndName(surnameAndName)
    } yield CompetitorResult(
      surnameFormat(surname),
      dnlc(name),
      dnlc(region),
      List(dnlc(penaltyProne).toInt, dnlc(penaltyStanding).toInt),
      DeciSecond(dnlc(timeTotal)),
      List(timeRun1, timeRun2, timeRun3).map(x => DeciSecond(dnlc(x))),
      List(timeShootingProne, timeShootingStanding).map(x => DeciSecond(dnlc(x))),
      position = dnlc(position).toInt,
      isTimeShootingValid)}.toList
  }

  def getCompetitorsResults(pdf: PDF, debug: Boolean = false) = {
    val string = stringPreparation(pdf)
    if (debug) println(string)
    createCompetitorsResults(string)
  }
}
