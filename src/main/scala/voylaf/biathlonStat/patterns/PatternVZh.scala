package voylaf.biathlonStat.patterns

import voylaf.biathlonStat.biathlon.{CompetitorResult, DeciSecond}
import voylaf.biathlonStat.parsing.{PDF, RaceAnalysisParsing}

trait PatternVZh extends RaceAnalysisParsing {
  val minBlock = s"""($sectionTime)$sectionTimeGap$positionNumber"""
  val minBlocks5 = minBlock * 5
  val lap12 = s"""$otherString$minBlocks5"""
  val lap3 = s"""$otherString$minBlock$minBlock"""
  val blockTimes = s"""($sectionTime)$sectionTimeGap$lap12$lap12$lap3"""
  val blockPattern = (blockHead + blockTimes).r

  val isTimeShootingValid = true

  /*
   * fix strings as "1 2",
   * where "1" is penalty1, "2" is penalty2
   * to "1 \n2"
   */
  def fixInvalid(list: List[String]): List[String] = {
    val invalidStringPattern = """[0-9][ ][0-9]\n"""
    fixInvalidStrings(list, invalidStringPattern, """[0-9][ ]""", """[0-9]\n""")
  }

  def createCompetitorsResults(string: String) = {
    for {
      blockPattern(position, surnameAndName, regionA, penaltyProne, penaltyStanding, timeTotal, timeRun1, timeShootingProne,
        _, _, _, timeRun2, timeShootingStanding, _, _, _, timeRun3, _) <- blockPattern.findAllIn(string)

      (surname, name, region) = {
        if (regionA != null) {
          val (surname, name) = separateSurnameAndName(surnameAndName)
          (surname, name, regionA)
        } else ("Bad data", "Bad data\n", "Bad data\n")
      }
    } yield CompetitorResult(
      surnameFormat(surname),
      dnlc(name),
      dnlc(region),
      penalty = List(dnlc(penaltyProne).toInt, dnlc(penaltyStanding).toInt),
      timeTotal = DeciSecond(dnlc(timeTotal)),
      timeRun = List(timeRun1, timeRun2, timeRun3).map(x => DeciSecond(dnlc(x))),
      timeShooting = List(timeShootingProne, timeShootingStanding).map(x => DeciSecond(dnlc(x))),
      position = dnlc(position).toInt,
      isTimeShootingValid)
  }.toList

  def getCompetitorsResults(pdf: PDF, debug: Boolean = false) = {
    val string = stringPreparation(pdf)
    if (debug) println(string)
    createCompetitorsResults(string)
  }

  val badData = "Bad data"

  def notEqualBadData(surname: String, name: String, region: String) = {
    name != badData && surname != badData && region != badData
  }
}
