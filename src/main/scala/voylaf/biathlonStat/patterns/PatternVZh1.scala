package voylaf.biathlonStat.patterns

/*
 * December, 2016
 * example:
 * 3 //position
70
НЕЧКАСОВА Галина //surname and name
Новосибирская область //region
0 2 //penalty
2
25:05.9 //timeTotal
+42.3
Круг 1
7:41.4 //timeRun1
+21.3
9
32.3 //timeShootingProne
+4.0
10
54.1
+4.5
9
6.1
8:41.6
+0.1
2
8:41.6
+0.1
2
Круг 2
7:31.0 //timeRun2
+5.9
4
33.3 //timeShootingStanding
+7.8
20
50.1
+6.0
9
1:00.4
9:21.5
+55.7
13
18:03.1
+55.8
4
Круг 3
7:02.8 //timeRun3
+13.9
5
7:02.8
+13.9
5
Итог
22:15.2
+25.8
4
1:05.6
+11.1
14
1:44.2
+10.5
8
1:06.5
 */
trait PatternVZh1 extends PatternVZh {
  override val minBlocks5 = minBlock * 3 + s"$sectionTime" + minBlock * 2
  override val lap12 = s"""$otherString$minBlocks5"""
  override val blockTimes = s"""($sectionTime)$sectionTimeGap$lap12$lap12$lap3"""
  override val blockPattern = (blockHead + blockTimes).r
}
