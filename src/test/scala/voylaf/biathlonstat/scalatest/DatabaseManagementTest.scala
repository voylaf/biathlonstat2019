package voylaf.biathlonstat.scalatest

import org.scalatest.fixture.FlatSpec
import scalikejdbc._
import scalikejdbc.scalatest.AutoRollback
import voylaf.biathlonStat.biathlon.RaceStat
import voylaf.biathlonStat.database.DatabaseManagement
import voylaf.biathlonStat.database.DatabaseManagement._
import voylaf.biathlonStat.parsing.PDFParceSprint

/*
 * http://scalikejdbc.org/documentation/testing.html
 * AutoRollback trait provides automatic rollback after each test and data fixture.
 */
class DatabaseManagementTest extends FlatSpec with AutoRollback {

  val path = """C:\Development\files\Biathlon Russian Cup\2016-2017\test\"""
  val pdfName = "20170406sprintw_123=37.pdf"
  case class A(pdfName: String, path: String) extends PDFParceSprint
  val race = A(pdfName, path).race
  val raceStat = new RaceStat(race)

  ConnectionPool.add('bstat, "jdbc:postgresql:BiathlonStat", "scalaApps", "1")
  override def db = NamedDB('bstat).toDB

  def countSprints()(implicit session: DBSession) = sql"select count (*) from sprints".map(_.int(1)).single.apply().get

  override def fixture(implicit session: DBSession) {
    val id = DatabaseManagement.addCompetition(race).toInt
    println(s"competition $id")

    val newNames = DatabaseManagement.findNewNames(raceStat.competitorsResultStat.map(_.competitorResult))
    DatabaseManagement.insertNewNames(newNames, race.competitors.isMale, true)

    val namesIds = namesId(raceStat.competitorsResultStat)

    DatabaseManagement.addRaceStat(raceStat, id)
    DatabaseManagement.addAliases()
  }

  val countBefore = countSprints()

  behavior of "sprints"

  "test1" should "get id" in { implicit session =>
    assert(true)
  }

  "inserted results in sprints" should "> 0" in { implicit session =>
    val countAfter = countSprints()
    assert(countAfter > countBefore)
  }

  behavior of "names_display"

  """For Телицина Валентина""" should "be 1 id" in { implicit session =>
    val alias = ("Телицина", "Валентина")
    assert(findBiatleteByAlias(alias).size == 1)
  }

  "Name changer" should "works correctly" in { implicit session =>
    val alias = ("Назарова", "Валентина")
    val changer = Changer(("Телицина", "Валентина"), alias)
    changeAlias(changer)
    assert(findBiatleteByAlias(alias).size == 1)
  }

  def deletedCount()(implicit session: DBSession): Int = sql"select count(biathlete_id) from biathletes where is_deleted = true"
      .map(_.int(1)).single.apply().get
  /*def aliasesCount(alias: (Surname, Name))(implicit session: DBSession): Int = {
    val (surname, name) = alias
    sql"select count id from names_display where ".map(_.int(1)).single.apply().get
  }*/
      
  
  "merge biathletes" should "be smaller by 1" in { implicit session =>
    val changer = Changer(("Бахтина", "Карина"), ("Куклина", "Лариса"))
    val before = deletedCount()
    
    mergeBiathletes(changer)
    val after = deletedCount()
    
    assert(before + 1 == after)
  }
  
  //todo: добавить тест insertNewNames для недобавленных сразу биатлонистов 
  
}