package voylaf.biathlonstat.scalatest

import org.scalatest.FlatSpec
import voylaf.biathlonStat.biathlon.DeciSecond

class DecisecondTest extends FlatSpec {
  val min1 = DeciSecond(1, 0, 0)
  "1 minute" should "equal 600 ds" in {
    val ds600 = DeciSecond(600)
    assert(min1.i == ds600.i)
  }
  
  val time2 = DeciSecond(2, 2, 2, 2)
  "2 hours 2 minutes 2 seconds 2 deciseconds" should "equal 73222 ds" in {
    val ds73222 = DeciSecond(73222)
    assert(time2.i == ds73222.i)
  }
  
  val compare = min1 compare time2
  "1 minute" should "less than 2 hours 2 minutes 2 seconds 2 deciseconds" in {
    assert(compare == -1)
  }
  
  val time3 = DeciSecond(7, 47, 3, 5)
  "5 hours 7 minutes 47 seconds 3 deciseconds in string" should "be 5:07:47.3" in {
    val toString = "5:07:47.3"
    assert (time3.toString() == toString)
  }
  
  val time4 = DeciSecond(7, 47, 3)
  "7 minutes 47 seconds 3 deciseconds in string" should "be 7:47.3" in {
    val toString = "7:47.3"
    assert (time4.toString() == toString)
  }
  
  val time5 = DeciSecond("2:22:38.9")
  "2 hours 22 minutes 38 seconds 9 deciseconds" should "equal 85589 deciseconds" in {
    assert(time5.i == 85589)
  }
  
  val time6 = DeciSecond("32:15.3")
  "32:15.3" should "equal 19353 deciseconds" in {
    assert(time6.i == 19353)
  }
}