package voylaf.biathlonstat.scalatest

import org.scalatest.{BeforeAndAfter, FlatSpec}
import voylaf.biathlonStat.biathlon.CompetitorResultStat2
import voylaf.biathlonStat.fpParsing.Handlers
import voylaf.biathlonStat.parsing.PDF

class ParseRaceTest extends FlatSpec with BeforeAndAfter {

  import voylaf.biathlonStat.parsing.ParseRace._

  //todo: failed load from file
  val testDir = """C:\Development\files\biathlonStat\testDir\"""
  val fileSprint123 = "20161201sprintm_123.pdf"

  //todo: work with \n - wrap by whitespace needed
  def pdf2String: PDF => String = (pdf: PDF) => pdf.result

  val pdfSprint123: String = pdf2String(new PDF(testDir, fileSprint123))

  "Date in string" should "be 01 ДЕК 2016" in {
    val res = parseDate(pdfSprint123)
    assert(res === Right(Handlers.string2Date("01", "ДЕК", "2016")))
  }

  "tt" should "fa" in {
    val s = "date not found in КУБОК РОССИИ 1 ЭТАП ПО БИАТЛОНУ      УВАТ (РОССИЯ) Начало: 12:4"
    val r = "[0-9]{2}".r
    val res = r.split(s)
    assert(res(1) === ":4")
  }

  val mainStringEx1 = "1 59 СЛЕПЦОВА Светлана ХМАО-Югра 0 0 1 0 1 52:27.0 0.0"
  val mainStringEx2 = "3 58 УСЛУГИНА Ирина Тюменская область 4 4 2 0 10 1:02:27.0 10.0"

  "Main string" should "Слепцова Светлана" in {
    val answer = CompetitorResultStat2("1", "59", "СЛЕПЦОВА", "Светлана", "ХМАО-Югра", "0 0 1 0 1", "52:27.0")
    val res = parseMainString(mainStringEx1)(4)
    assert(res === Right(answer))
  }

  "Main string" should "Услугина Ирина" in {
    val answer = CompetitorResultStat2("3", "58", "УСЛУГИНА", "Ирина", "Тюменская область", "4 4 2 0 10", "1:02:27.0")
    val res = parseMainString(mainStringEx2)(4)
    assert(res === Right(answer))
  }
}
