package voylaf.biathlonstat.scalatest

import org.scalatest.FlatSpec
import voylaf.biathlonStat.parsing.{IO, PDFParceSprint}

class PatternCommonTest extends FlatSpec {
  val path = """C:\Development\files\Biathlon Russian Cup\2016-2017\test\"""
  val pdfs = IO.allPdfInDir(path)
  /*
   * PatternVZh ломается на 90 ИЛЬИН Илья Томская область 3 1 из 20170325sprintm_vz=104
   * Будет ломаться еще на ком-то, стоит переделать шаблон
   */

  class A(val pdfName: String, val path: String) extends PDFParceSprint
  val allPdfs = pdfs.map { p =>
    val a = new A(p, path)
    a.competitorsResults()
  }
  
  "tested competiton results" should "not contains empty values" in {
    assert {
      allPdfs.flatten.forall{ x => 
        List(x.surname, x.name, x.region).map(y => y != "").foldLeft(true)(_ && _)
      }
    }
  }
}