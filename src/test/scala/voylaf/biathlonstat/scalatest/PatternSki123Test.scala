package voylaf.biathlonstat.scalatest

import org.scalatest.FlatSpec
import voylaf.biathlonStat.patterns.PatternSki123

class PatternSki123Test extends FlatSpec with PatternSki123 {
  /*val pdfTest2 = new PDF("""L:\Downloads\""", """250117_analysis_m.pdf""")
  val pdfTest = new PDF("""L:\Downloads\""", """analysis_btm_12122016.pdf""")
  val pdfTest3 = new PDF("""C:\Development\files\Biathlon Russian Cup\""", """20161201sprintm123.pdf""")
  val pdfTest4 = new PDF("""C:\Development\files\Biathlon Russian Cup\""", """20161201sprintw123.pdf""")
  //val pdfTest5 = new PDF("""C:\Development\files\Biathlon Russian Cup\""", """20161210sprintm123.pdf""")

  val competitorsResults = getCompetitorsResults(pdfTest)
  val competitorsResults2 = getCompetitorsResults(pdfTest2)
  val competitorsResults3 = getCompetitorsResults(pdfTest3)
  val competitorsResults4 = getCompetitorsResults(pdfTest4)
  //val competitorsResults5 = getCompetitorsResults(pdfTest5)

  "250117_analysis_m" should "contain 65 biathlets" in {
    assert(competitorsResults2.size == 65)
  }

  "250117_analysis_m" should "not contain empty values" in {
    assert {
      competitorsResults2.forall { cr =>
        cr.name != "" && cr.surname != "" && cr.region != ""
      }
    }
  }

  "analysis_btm_12122016" should "contain 68 biathlets" in {
    assert(competitorsResults.size == 68)
  }

  "analysis_btm_12122016" should "not contain empty values" in {
    assert {
      competitorsResults.forall { cr =>
        cr.name != "" && cr.surname != "" && cr.region != ""
      }
    }
  }*/
}