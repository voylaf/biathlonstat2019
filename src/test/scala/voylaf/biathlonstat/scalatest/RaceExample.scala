package voylaf.biathlonstat.scalatest

import voylaf.biathlonStat.biathlon._

object RaceExample {
  /*
   * Chaikovski, 12 December 2016, women, soft by Vasiliy Zhizhin
   */
  val b1 = CompetitorResult(
    surname = "Куклина",
    name = "Лариса",
    region = "Ямало-Ненецкий АО",
    penalty = List(0, 0),
    timeTotal = DeciSecond(24, 23, 6),
    timeRun = List(DeciSecond(7, 46, 2), DeciSecond(7, 36, 4), DeciSecond(7, 16, 3)),
    timeShooting = Nil,
    position = 1)

  val b2 = CompetitorResult(
    surname = "Алексешникова",
    name = "Александра",
    region = "Новосибирская область - Кемеровская область",
    penalty = List(0, 1),
    timeTotal = DeciSecond(24, 56, 0),
    timeRun = List(DeciSecond(7, 46, 6), DeciSecond(7, 29, 2), DeciSecond(7, 2, 2)),
    timeShooting = Nil,
    position = 2)

  val b3 = CompetitorResult(
    surname = "Нечкасова",
    name = "Галина",
    region = "Новосибирская область",
    penalty = List(0, 2),
    timeTotal = DeciSecond(25, 5, 9),
    timeRun = List(DeciSecond(7, 41, 4), DeciSecond(7, 31, 0), DeciSecond(7, 2, 8)),
    timeShooting = Nil,
    position = 3)

  val b4 = CompetitorResult(
    surname = "Закурдаева",
    name = "Виолетта",
    region = "Красноярский край",
    penalty = List(0, 0),
    timeTotal = DeciSecond(25, 13, 8),
    timeRun = List(DeciSecond(7, 49, 5), DeciSecond(7, 53, 2), DeciSecond(7, 25, 4)),
    timeShooting = Nil,
    position = 4)

  val competitorsResult = List(b1, b2, b3, b4)

  val race = Race(
    softVersion = VZhizhin,
    level = RussianCup,
    format = Sprint,
    name = "raceStatTest1",
    date = new java.util.Date(1L),
    competitors = Men,
    competitorsResult)

  val b1s = CompetitorResultStat(b1, 3, List(0), List(0), Nil, Nil)
  val b2s = CompetitorResultStat(b2, 2, List(0), List(1), Nil, Nil)
  val b3s = CompetitorResultStat(b3, 1, List(0), List(2), Nil, Nil)
  val b4s = CompetitorResultStat(b4, 4, List(0), List(0), Nil, Nil)
  
  val competitorsResultStatShouldBe = List(b1s, b2s, b3s, b4s)

}