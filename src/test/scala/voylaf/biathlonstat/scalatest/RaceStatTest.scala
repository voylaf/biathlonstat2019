package voylaf.biathlonstat.scalatest

import org.scalatest.FlatSpec
import voylaf.biathlonStat.biathlon.RaceStat

class RaceStatTest extends FlatSpec {
  val race = RaceExample.race
  val competitorsResult = race.competitorsResult
  val raceStat = new RaceStat(race)
  val competitorsResultStat = raceStat.competitorsResultStat
  
  "sportsmen" should "be sorted by time run" in {
    val sortedByTimeRun = raceStat.sortByTimeRun(competitorsResult)
    val surnames = sortedByTimeRun.map(_.surname)
    val shouldBe = List("Нечкасова", "Алексешникова", "Куклина", "Закурдаева")
    assert(surnames == shouldBe)
  }
  
  "competitorsResultStat" should "be as in example" in {
    assert(competitorsResultStat == RaceExample.competitorsResultStatShouldBe)
  }
}