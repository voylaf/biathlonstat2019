package voylaf.biathlonstat.scalatest.fpParsing

import org.scalatest.FunSpec
import voylaf.biathlonStat.biathlon._
import voylaf.biathlonStat.fpParsing.AnalysisParser._
import voylaf.biathlonStat.fpParsing._
import voylaf.biathlonStat.parsing.PDF

class AnalysisParserTest extends FunSpec {

  implicit val parsers: Sliceable.type = Sliceable
  //todo: errors handling
  //todo: beforeAndAfter
  val testDir = """C:\Development\files\biathlonStat\testDir\"""
  val fileSprint123 = "20161201sprintm_123"
  val fileSprintVZ = "20161212sprintw_vz1"
  val fileSprintVZR = "20161226sprintw_vz"
  val fileIndividual123 = "20161210individualm_123"
  val fileIndividualVZ = "20161210individualw_vz"

  //todo: work with \n - wrap by whitespace needed
  def pdf2String: PDF => String = (pdf: PDF) => pdf.result

  def prepareTestData(dir: String, fileName: String): String = {
    pdf2String(new PDF(dir, fileName + ".pdf"))
  }

  val pdfSprint123: String = prepareTestData(testDir, fileSprint123)
  val pdfSprintVZ: String = prepareTestData(testDir, fileSprintVZ)
  val pdfSprintVZR: String = prepareTestData(testDir, fileSprintVZR)
  val pdfIndividual123: String = prepareTestData(testDir, fileIndividual123)
  val pdfIndividualVZ: String = prepareTestData(testDir, fileIndividualVZ)

  lazy val primitive = "КУБОК РОССИИ 1  \n ЭТАП ПО БИАТЛОНУ СПРИНТ dsgiagji;\n  Ski 123"

  describe("Race format") {
    val parser = parseCompetitionFormat
    describe("of sprint 123") {
      val res = Sliceable.run(parser)(pdfSprint123)
      it("should be Sprint") {
        assert(res === Right(Sprint))
      }
    }

    describe("of individual 123") {
      val res = Sliceable.run(parser)(pdfIndividual123)
      it("should be Individual") {
        assert(res === Right(Individual))
      }
    }

    describe("of individual vz") {
      val res = Sliceable.run(parser)(pdfIndividualVZ)
      it("should be Individual") {
        assert(res === Right(Individual))
      }
    }
  }

  describe("Soft version") {
    describe("of sprint 123") {
      val parser = parseSoft
      val res = Sliceable.run(parser)(pdfSprint123)
      it("should be Ski123") {
        assert(res === Right(Ski123))
      }
    }
  }

  describe("Date in string") {
    describe("of sprint 123") {
      val parser = parseDate
      val res = Sliceable.run(parser)(pdfSprint123)
      it("should be 01 ДЕК 2016") {
        assert(res === Right(Handlers.string2Date("01", "ДЕК", "2016")))
      }
    }

    val dateEx1 = "10 TTslk 01 ДЕК 2017 SSeT"
    describe("of dateEx1") {
      val parser = parseDate
      val res = Sliceable.run(parser)(dateEx1)
      it("should be 01 ДЕК 2017") {
        assert(res === Right(Handlers.string2Date("01", "ДЕК", "2017")))
      }
    }

    val dateEx2 = "10 TTslk sf 2017 SSeT"
    describe("failure") {
      val parser = parseDate
      val res = Sliceable.run(parser)(dateEx2)
      it("should be end of file") {
        res.left.map(pe => assert(pe.latest.contains((Location(dateEx2, dateEx2.length + 1), "end of file"))))
      }
    }
  }

  describe("Competitors") {
    describe("of sprint 123") {
      val parser = parseCompetitors
      val res = Sliceable.run(parser)(pdfSprint123)
      it("should be Men") {
        assert(res === Right(Men))
      }
    }
  }

  describe("Recursive search") {
    describe("in the phrase") {
      val parser = parseDate
      val res = Sliceable.run(parser)("fal;skjf2450 rwitqjqortj4 q4q90")
      it("should ends correctly") {
        res.isLeft
      }
    }
  }

  describe("Laconic biathlete result") {
    describe("of string") {

      it("should be Слепцова Светлана") {
        val answer = LaconicBiathleteResult("СЛЕПЦОВА", "Светлана", "ХМАО-Югра ", List("0", "0", "1", "0"), "1", "52:27.0")
        val parser = parseLaconicBiathleteResult(4)
        val mainStringEx1 = "1 59 СЛЕПЦОВА Светлана ХМАО-Югра 0 0 1 0 1 52:27.0 0.0"
        val res = Sliceable.run(parser)(mainStringEx1)
        assert(res === Right(answer))
      }

      it("should be Услугина Ирина") {
        val answer = LaconicBiathleteResult("УСЛУГИНА", "Ирина", "Тюменская область ", List("4", "4", "2", "0"), "10", "1:02:27.0")
        val parser = parseLaconicBiathleteResult(4)
        val mainStringEx2 = "3 58 УСЛУГИНА Ирина Тюменская область 4 4 2 0 10 1:02:27.0 10.0"
        val res = Sliceable.run(parser)(mainStringEx2)
        assert(res === Right(answer))
      }
    }

    describe("recursive of sprint 123") {
      it("should be Буртасов Максим") {
        val answer = LaconicBiathleteResult("БУРТАСОВ", "Максим", "Новосибирская область ", List("0", "0"), "0", "26:29.7")
        val parser = recEx(parseLaconicBiathleteResult(2))
        val res = Sliceable.run(parser)(pdfSprint123)
        assert(res === Right(answer))
      }
    }
  }

  describe("Headers") {
    val parser = parseHeaders

    describe("in the phrase") {
      it("should be Lap1 Lap2 (1)") {
        val answer = List(Lap1, Lap2)
        val headerEx1 = "Описание Круг 1 Круг 2 "
        val res1 = Sliceable.run(parser)(headerEx1)
        assert(res1 === Right(answer))
      }

      it("should be Lap1 Lap2 (2)") {
        val answer = List(Lap1, Lap2)
        val headerEx2 = "Круг 1 Круг 2"
        val res1 = Sliceable.run(parser)(headerEx2)
        assert(res1 === Right(answer))
      }
    }

    describe("of sprint123") {
      it("should be Lap1, Lap2, Lap3, Lap4, Lap5") {
        val answer = List(Lap1, Lap2, Lap3, Lap4, Lap5)
        val res1 = Sliceable.run(parser)(pdfSprint123)
        assert(res1 === Right(answer))
      }
    }

    describe("of sprintVZ") {
      it("should be LapTime, SpeedTime, ShootingTime, FiringLineTime, PenaltyTime, LapTime, SummaryTime") {
        val answer = List(LapTime, SpeedTime, ShootingTime, FiringLineTime, PenaltyTime, LapTime, SummaryTime)
        val res1 = Sliceable.run(parser)(pdfSprintVZR)
        assert(res1 === Right(answer))
      }
    }
  }

  describe("Cells") {
    describe("Cell1") {
      val parser = parseCell1
      describe("in phrase") {
        it("should be 7:29.1") {
          val cell1Ex = "7:29.1"
          val answer = Cell1("7:29.1")
          val res1 = Sliceable.run(parser)(cell1Ex)
          assert(res1 === Right(answer))
        }
      }
    }

    describe("Cell3") {
      val parser = parseCell3
      describe("in phrase") {
        it("should be 52.2 +10.5 =2") {
          val cell3Test1 = "52.2 +10.5 =2 "
          val answer = Cell3.fromList(cell3Test1.split(' ').toList)
          val res1 = Sliceable.run(parser)(cell3Test1)
          assert(res1 === Right(answer))
        }

        it("should be 10:45.5 +1:02.3 11 ") {
          val cell3Test2 = "10:45.5 +1:02.3 11 "
          val answer = Cell3.fromList(cell3Test2.split(' ').toList)
          val res1 = Sliceable.run(parser)(cell3Test2)
          assert(res1 === Right(answer))
        }
      }
    }
  }

  describe("Row") {
    describe("in phrases") {
      val parser = parseRow
      it("should be correct (1)") {
        val rowEx1 = "Круг 1 7:29.1 +9.0 2 41.3 +13.0 34 1:06.3 +16.7 39 1:00.7 9:36.1 +54.6 27 9:36.1 +54.6 27 "
        val cells = List(
          Cell3("7:29.1", "+9.0", "2"),
          Cell3("41.3", "+13.0", "34"),
          Cell3("1:06.3", "+16.7", "39"),
          Cell1("1:00.7"),
          Cell3("9:36.1", "+54.6", "27"),
          Cell3("9:36.1", "+54.6", "27")
        )
        val answer = Row(Lap1, cells)
        val res1 = Sliceable.run(parser)(rowEx1)
        assert(res1 === Right(answer))
      }

      it("should be correct (2)") {
        val rowEx2 = "Круг 1 7:29.1 +9.0 2 "
        val cells = List(
          Cell3("7:29.1", "+9.0", "2")
        )
        val answer = Row(Lap1, cells)
        val res1 = Sliceable.run(parser)(rowEx2)
        assert(res1 === Right(answer))
      }
    }

    describe("in sprint vz") {
      val parser = recEx(parseRow)
      it("should be correct") {
        val cells = List(
          Cell3("7:46.2", "+26.1", "15"),
          Cell3("28.8", "+0.6", "2"),
          Cell3("49.6", "0.0", "1"),
          Cell1("5.7"),
          Cell3("8:41.5", "0.0", "1"),
          Cell3("8:41.5", "0.0", "1")
        )
        val answer = Row(Lap1, cells)
        val res1 = Sliceable.run(parser)(pdfSprintVZ)
        assert(res1 === Right(answer))
      }
    }
  }

  /* нужны другие тесты
   */
  //  describe("Table") {
  //    val parser = Sliceable.rec(parseTable)
  //    describe("in sprint 123") {
  //      it("should consist of 4 rows") {
  //        val res = Sliceable.run(parser)(pdfSprint123)
  //        res.map(_.rows foreach println)
  //        val rowsCount = res.map(_.rows.length)
  //        assert(rowsCount === Right(4))
  //      }
  //    }
  //
  //    describe("in sprint vz") {
  //      it("should consist of 4 rows") {
  //        val res = Sliceable.run(parser)(pdfSprintVZR)
  //        res.map(_.rows foreach println)
  //        val rowsCount = res.map(_.rows.length)
  //        assert(rowsCount === Right(4))
  //      }
  //    }
  //  }

  //todo: make test
  describe("Verbose biathlete result") {
    describe("in sprint 123") {
      val parser = recEx(parseVerboseBiathleteResult(2))
      it("should contain Буртасов Максим data") {
        val res = Sliceable.run(parser)(pdfSprint123)
        res.foreach(vbr => assert(vbr.laconic.surname === "БУРТАСОВ" && vbr.laconic.name === "Максим"))
      }
    }

    describe("search by beginning") {
      val parser = recEx(parseResults(2))
      describe("sprint 123") {
        val res = Sliceable.run(parser)(pdfSprint123)
        res.foreach(ls => ls.length === 70)
      }
    }

    //    describe("debug") {
    //      val parser = parseResults(2)
    //      describe("files") {
    //        it("println (1)") {
    //          val res = Sliceable.run(parser)(pdfSprint123)
    //          res.map { x =>
    //            x foreach println
    //          }
    //        }
    //        it("println (2)") {
    //          val res = Sliceable.run(parser)(pdfSprintVZR)
    //          res.map { x =>
    //            x foreach println
    //          }
    //        }
    //      }
    //    }
  }

  describe("block") {
    val parser = parseBlock(2)
    //    describe("merge blocks") {
    //      it("should be right in my test") {
    //        //scalacheck generator needed
    //      }
    //    }

    //    describe("blocks") {
    //      val parser = parseBlocks(2)
    //      val res = Sliceable.run(parser)(pdfSprint123)
    //      it("println") {
    //        res.map(_ foreach println)
    //      }
    //    }

    describe("1 block") {
      describe("sprint123") {
        val res = Sliceable.run(parser)(pdfSprint123)
        describe("first block") {
          it("is right") {
            assert(res.isRight)
          }
          it("rows are Nil") {
            res.map(x => assert(x.rows.isEmpty))
          }
          it("biathletes count is 9") {
            res.map(x => assert(x.vbrs.length == 9))
          }
        }

        describe("second block") {
          val parser2 = Sliceable.flatMap(parseBlock(2))(_ => parseBlock(2))
          val res2 = Sliceable.run(parser2)(pdfSprint123)
          it("is right") {
            assert(res2.isRight)
          }
          it("rows aren't Nil") {
            res2.map(x => assert(x.rows.nonEmpty))
          }
          it("biathletes count is 10") {
            res2.map(x => assert(x.vbrs.length == 10))
          }
        }
      }
    }
  }

  describe("results") {

    val parser = parseResults

    describe("count should be right in") {
      def testCounts(file: String, count: Int): Unit = {
        val res = Sliceable.run(parser)(file)
        assert(res.isRight)
        res.foreach(ls => assert(ls._2.length === count))
      }

      it("sprint 123") {
        testCounts(pdfSprint123, 70)
      }

      it("sprint vzh") {
        testCounts(pdfSprintVZ, 63)
      }

      it("individual 123") {
        testCounts(pdfIndividual123, 66)
      }

      it("individual vz") {
        testCounts(pdfIndividualVZ, 58)
      }
    }

    describe("laps speed count should be right in") {
      def approximatelyRight(errorsMaxPercent: Int)(goodAndBad: (List[_], List[_])): Boolean = {
        val (good, bad) = goodAndBad
        val errorsPercent = bad.length / (bad.length + good.length).toDouble
        println(errorsPercent)
        errorsPercent < errorsMaxPercent / 100D
      }

      def testCounts(file: String): Unit = {
        val res = Sliceable.run(parser)(file)
        assert(res.isRight)
        res.foreach { case (cf, ls) =>
          ls foreach println
          assert(
            approximatelyRight(10)(
              ls.partition(br => br.speedTimes.map(_.length).contains(cf.lapsCount)))
          )
        }
      }

      it("sprint 123") {
        testCounts(pdfSprint123)
      }

      it("sprint vzh") {
        testCounts(pdfSprintVZ)
      }

      it("individual 123") {
        testCounts(pdfIndividual123)
      }

      it("individual vz") {
        testCounts(pdfIndividualVZ)
      }
    }

    //doesn't work: penalty time not included
    describe("summary time should be speed time + firing line time in") {
      def summaryEquals(file: String): Unit = {
        val res = Sliceable.run(parser)(file)
        assert(res.isRight)
        res.foreach { case (cf, ls) =>
          assert(ls.forall { br =>
            val sum = for {
              st <- br.speedTimes
              ft2 <- br.firingLineStandingTimes
              ft1 <- br.firingLineProneTimes
            } yield DeciSecond(st.map(_.i).sum + ft1.map(_.i).sum + ft2.map(_.i).sum)
            println(s"$sum ${br.summaryTime}")
            sum.contains(br.summaryTime)
          })
        }
      }

      it("sprint 123") {
        summaryEquals(pdfSprint123)
      }

      it("sprint vzh") {
        summaryEquals(pdfSprintVZ)
      }

      it("individual 123") {
        summaryEquals(pdfIndividual123)
      }

      it("individual vz") {
        summaryEquals(pdfIndividualVZ)
      }
    }
  }

}
