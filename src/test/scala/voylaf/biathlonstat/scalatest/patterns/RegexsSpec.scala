package voylaf.biathlonstat.scalatest.patterns

import org.scalatest.FunSpec
import voylaf.biathlonStat.parsing.Regexs._

import scala.util.matching.Regex

class RegexsSpec extends FunSpec {
  def test(s: String)(r: Regex): Option[String] = {
    r.findFirstIn(s)
  }

  describe("surname and name") {
    val testString1 = "=15 5 89 СЛЕПЦОВА Светлана вфощщц"
    val testString2 = "=15 5 89 светлана ывфощщц"
    val testString3 = "=15 5 89 СЛЕПЦОВА cветлана вфощщц"
    val testString4 = "=15 5 89 ДЕНЬ 85 СЛЕПЦОВА Светлана вфощщц"

    val regex: Regex = allP.group ~ surnameP ~ nameP

    def surnameAndName: String => Option[(String, String)] = { s =>
      val opt = test(s)(regex)
      opt.map {
        case regex(_, surname, name) => (surname, name)
        case x => (s"fail for $x", "")
      }
    }

    it("recognise valid surname and name") {
      assert(surnameAndName(testString1) === Some(("СЛЕПЦОВА", "Светлана")))
    }

    it("recognise valid surname and name4") {
      assert(surnameAndName(testString4) === Some(("СЛЕПЦОВА", "Светлана")))
    }

    it("recognise invalid surname and name: светлана") {
      assert(surnameAndName(testString2) === None)
    }

    it("recognise invalid surname and name: СЛЕПЦОВА cветлана") {
      assert(surnameAndName(testString2) === None)
    }
  }

}
